# mojolicious

[Mojolicious](https://metacpan.org/pod/Mojolicious) Web Application Framework. [mojolicious.org](https://mojolicious.org)

# Semi-official documentation
* [*Mojolicious and DBIx::Class*
  ](https://mojolicious.io/blog/2019/02/18/mojolicious-and-dbix-class/)
  2019-02 Doug Bell
  * DBIx::Class in Debian : libdbix-class-perl
* [*Day 1: Getting Started*
  ](https://mojolicious.io/blog/2017/12/01/day-1-getting-started/)
  2017-12 Joel Berger

# Plugins
* [Distributions Which Depend on Mojolicious
  ](https://metacpan.org/requires/distribution/Mojolicious)
  CPAN
* [Mojolicious plugins at Debian](https://packages.debian.org/en/libmojolicious-plugin-)

![Mojolicious plugins at Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=libmojolicious-plugin-assetpack-perl%20libmojolicious-plugin-authentication-perl%20libmojolicious-plugin-authorization-perl%20libmojolicious-plugin-basicauth-perl%20libmojolicious-plugin-bcrypt-perl%20libmojolicious-plugin-cgi-perl%20libmojolicious-plugin-i18n-perl%20libmojolicious-plugin-mailexception-perl%20libmojolicious-plugin-oauth2-perl%20libmojolicious-plugin-openapi-perl%20libmojolicious-plugin-renderfile-perl&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)

![Mojolicious authorisation's plugins at Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=libmojolicious-plugin-authorization-perl%20libmojolicious-plugin-basicauth-perl%20libmojolicious-plugin-oauth2-perl&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)
